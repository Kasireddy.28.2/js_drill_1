let dataSet=require("./data");

let cityAndCountryOfIndividuals={};

for(let data of dataSet){
    cityAndCountryOfIndividuals[data.name]={};
    cityAndCountryOfIndividuals[data.name]["city"]=data.city;
    cityAndCountryOfIndividuals[data.name]["country"]=data.country;
};

console.log(cityAndCountryOfIndividuals);