let dataSet=require("./data");

function firstHobbyOfEachIndividual(array){
    let hobbyOfIndividual={};

    for(let data of array){
        hobbyOfIndividual[data.name]=data.hobbies[0];
    };

    return hobbyOfIndividual; 
};

let hobbyOfIndividual=firstHobbyOfEachIndividual(dataSet);

console.log(hobbyOfIndividual);