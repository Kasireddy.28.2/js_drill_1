let dataSet=require("./data");

function detailsOfIndividuals(array,position){
    let individualDetails={};

    for(let index=0;index<array.length;index++){

        if(index===position){
            individualDetails["name"]=array[index].name;
            individualDetails["city"]=array[index].city;
        }

    }

    return individualDetails;
};

console.log(detailsOfIndividuals(dataSet,3));