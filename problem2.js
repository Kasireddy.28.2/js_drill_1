let dataSet=require("./data");

function hobbies(array,age){
    let hobbiesOfIndividuals={};

    for(let data of array){
        if(data.age===age){
            hobbiesOfIndividuals[data.name]=data.hobbies;
        }
    }

    return hobbiesOfIndividuals;

};


let hobbiesOfIndividuals=hobbies(dataSet,30);

console.log(hobbiesOfIndividuals);