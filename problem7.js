let dataSet=require("./data");

function individualsAged25(array,age){
    let nameAndEmailOfIndivduals={};

    for (let data of dataSet){
        if(data.age===25){
            nameAndEmailOfIndivduals["name"]=data.name;
            nameAndEmailOfIndivduals["email"]=data.email;
        }
    }

    return nameAndEmailOfIndivduals;

};

let individualDetails=individualsAged25(dataSet,25);
console.log(individualDetails);