let dataSet=require("./data");

function emailIds(array){
    let emailIdsOfIndividuals={};

    for(let data of array ){
        emailIdsOfIndividuals[data.name]=data.email;
    }

    return emailIdsOfIndividuals;

};

let emailIdsOfIndividuals=emailIds(dataSet);

console.log(emailIdsOfIndividuals);